#!/usr/bin/env python
# coding=utf8

# This file is part of clPCI.
#
# clPCI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# clPCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with clPCI.  If not, see <http://www.gnu.org/licenses/>.
#
# (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>

import pyopencl as cl
import argparse
import sys
import numpy as np
import math
import time

WARMUP_MEAS = 10
NUM_MEAS = 2000

def init_cl(platform, device):
	if device != None:
		platforms = cl.get_platforms()
		if len(platforms) > 1 and platform == None:
			raise Exception('Found more then one platform, giving up.')
		platform = platforms[platform if platform != None else 0]
		properties = [(cl.context_properties.PLATFORM, platform)]
		devices = [platform.get_devices()[device]]
		ctx = cl.Context(devices, properties)
	else:
		ctx = cl.create_some_context()
	queue = cl.CommandQueue(ctx)
	return (ctx, queue)

def byte_size_2_float_size(floats):
	return floats / 4

class PlainMemory(object):
	def __init__(self, size, direction, ctx, queue):
		host_buf = np.empty(byte_size_2_float_size(size), dtype=np.float32)
		dev_buf = cl.Buffer(ctx, 0, size)
		if direction == 'h2d':
			self.src = host_buf
			self.dest = dev_buf
		elif direction == 'd2h':
			self.src = dev_buf
			self.dest = host_buf
		else:
			raise ValueError('unsupported copy direction "{0}"'.format(direction))
		self.queue = queue
		self.size = size

	def __call__(self):
		return cl.enqueue_copy(self.queue, self.dest, self.src, is_blocking=False)

class PinnedMemory(object):
	def __init__(self, size, direction, ctx, queue):
		self.pin_buf = cl.Buffer(ctx, cl.mem_flags.ALLOC_HOST_PTR, size)
		(host_buf, event) = cl.enqueue_map_buffer(queue, self.pin_buf, 0, 0, [byte_size_2_float_size(size)], dtype=np.float32, order='C', wait_for=None, is_blocking=True)
		dev_buf = cl.Buffer(ctx, 0, size)
		if direction == 'h2d':
			self.src = host_buf
			self.dest = dev_buf
		elif direction == 'd2h':
			self.src = dev_buf
			self.dest = host_buf
		else:
			raise ValueError('unsupported copy direction "{0}"'.format(direction))
		self.queue = queue
		self.size = size

	def __call__(self):
		return cl.enqueue_copy(self.queue, self.dest, self.src, is_blocking=False)

def benchmark(implementation):

	events = [implementation() for i in xrange(WARMUP_MEAS)]
	cl.wait_for_events(events)

	start = time.time()
	events = [implementation() for i in xrange(NUM_MEAS)]
	cl.wait_for_events(events)
	end = time.time()

	avg_host = (end - start) / len(events)
	bw_host = implementation.size / avg_host / 1e9

	print '{0:>20}   {1:>20.0f}   {2:>20.2f}'.format(implementation.size, avg_host * 1e6, bw_host)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Benchmark PCI bandwidth')
	parser.add_argument('-d', '--device', type=int, metavar='I', help='The device to use for the measurement')
	parser.add_argument('-p', '--platform', type=int, metavar='I', help='The platform to use for the measurement (requires device to be specified)')
	parser.add_argument('-s', '--size', type=int, metavar='BYTE', help='Memory size in byte', default=4*1024*1024)
	parser.add_argument('--dir', metavar='DIRECTION', help='The direction to copy, either h2d or d2h', default='h2d')
	parser.add_argument('-t', '--type', metavar='TYPE', help='The type of memory/transfer to used', default='plain')
	parser.add_argument('--sweep', action='store_true', default=False, help='Sweep the range of potential memory sizes')

	args = parser.parse_args()

	if args.dir not in ('h2d', 'd2h'):
		print 'The value of "--dir" must be either h2d or d2h.'
		sys.exit(1)

	(ctx, queue) = init_cl(args.platform, args.device)

	def make_implementation(size):
		if args.type == 'plain':
			return PlainMemory(size, args.dir, ctx, queue)
		elif args.type == 'pinned':
			return PinnedMemory(size, args.dir, ctx, queue)
		else:
			print 'The memory/transfer type "{0}" does not exist'.format(args.type)
			sys.exit(1)

	print '{0:>20}   {1:>20}   {2:>20}'.format('Buffer Size / B', 'Transfer Time / mus', 'Bandwidth / GB/s')
	if args.sweep:
		from sweep import sweep
		sweep(make_implementation, benchmark)
	else:
		implementation = make_implementation(args.size)
		benchmark(implementation)
