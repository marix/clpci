#!/usr/bin/env python
# coding=utf8

# This file is part of clPCI.
#
# clPCI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# clPCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with clPCI.  If not, see <http://www.gnu.org/licenses/>.
#
# (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>

import pyopencl as cl
import argparse
import sys
import numpy as np
import math
import time
import dummy

def init_cl(platform, deviceFrom, deviceTo):
	platforms = cl.get_platforms()
	if len(platforms) > 1 and platform == None:
		raise Exception('Found more then one platform, giving up.')
	platform = platforms[platform if platform != None else 0]
	properties = [(cl.context_properties.PLATFORM, platform)]
	platform_devices = platform.get_devices()
	ctx = cl.Context(platform_devices, properties)
	queueFrom = cl.CommandQueue(ctx, platform_devices[deviceFrom])
	queueTo = cl.CommandQueue(ctx, platform_devices[deviceTo])
	return (ctx, queueFrom, queueTo)

def byte_size_2_float_size(floats):
	return floats / 4

class Verifyable(object):
	def set_src(self, val):
		cl.enqueue_copy(self.queueFrom, self.from_buf, val)
	def get_dest(self):
		host_buf = np.empty(byte_size_2_float_size(self.size), dtype=np.float32)
		cl.enqueue_copy(self.queueTo, host_buf, self.to_buf)
		return host_buf

class Host2Host(object):
	def __init__(self, size, ctx):
		self.dummy_event = cl.UserEvent(ctx)
		self.dummy_event.set_status(cl.command_execution_status.COMPLETE)
		self.from_buf = np.empty(byte_size_2_float_size(size), dtype=np.float32)
		self.to_buf = np.empty(byte_size_2_float_size(size), dtype=np.float32)
		self.size = size

	def __call__(self, waitEvent = None):
		if waitEvent:
			waitEvent.wait()
		#np.copyto(self.to_buf, self.from_buf)
		self.to_buf[:] = self.from_buf
		return self.dummy_event

	def set_src(self, val):
		self.from_buf[:] = val

	def get_dest(self):
		return self.to_buf

class PlainMemoryBlocking(Verifyable):
	def __init__(self, size, ctx, queueFrom, queueTo):
		self.host_buf = np.empty(byte_size_2_float_size(size), dtype=np.float32)
		self.from_buf = cl.Buffer(ctx, 0, size)
		self.to_buf = cl.Buffer(ctx, 0, size)
		self.queueFrom = queueFrom
		self.queueTo = queueTo
		self.size = size

	def __call__(self, waitEvent = None):
		if waitEvent:
			waitEvent.wait()
		cl.enqueue_copy(self.queueFrom, self.host_buf, self.from_buf, is_blocking=True)
		return cl.enqueue_copy(self.queueTo, self.to_buf, self.host_buf, is_blocking=True)

class PlainMemoryAsync(Verifyable):
	def __init__(self, size, ctx, queueFrom, queueTo):
		self.host_buf = np.empty(byte_size_2_float_size(size), dtype=np.float32)
		self.from_buf = cl.Buffer(ctx, 0, size)
		self.to_buf = cl.Buffer(ctx, 0, size)
		self.queueFrom = queueFrom
		self.queueTo = queueTo
		self.size = size

	def __call__(self, waitEvent = None):
		wait_events = [waitEvent] if waitEvent else []
		to_host_event = cl.enqueue_copy(self.queueFrom, self.host_buf, self.from_buf, is_blocking=False, wait_for=wait_events)
		to_dev_event = cl.enqueue_copy(self.queueTo, self.to_buf, self.host_buf, is_blocking=False, wait_for=[to_host_event])
		queueFrom.flush()
		queueTo.flush()
		return to_dev_event

class PinnedMemoryBlocking(Verifyable):
	def __init__(self, size, ctx, queueFrom, queueTo):
		self.pin_buf = cl.Buffer(ctx, cl.mem_flags.ALLOC_HOST_PTR, size)
		(host_buf, event) = cl.enqueue_map_buffer(queueFrom, self.pin_buf, 0, 0, [byte_size_2_float_size(size)], dtype=np.float32, order='C', wait_for=None, is_blocking=True)
		self.host_buf = host_buf
		self.from_buf = cl.Buffer(ctx, 0, size)
		self.to_buf = cl.Buffer(ctx, 0, size)
		self.queueFrom = queueFrom
		self.queueTo = queueTo
		self.size = size

	def __call__(self, waitEvent = None):
		if waitEvent:
			waitEvent.wait()
		cl.enqueue_copy(self.queueFrom, self.host_buf, self.from_buf, is_blocking=True)
		return cl.enqueue_copy(self.queueTo, self.to_buf, self.host_buf, is_blocking=True)

class PinnedMemoryAsync(Verifyable):
	def __init__(self, size, ctx, queueFrom, queueTo):
		self.pin_buf = cl.Buffer(ctx, cl.mem_flags.ALLOC_HOST_PTR, size)
		(host_buf, event) = cl.enqueue_map_buffer(queueFrom, self.pin_buf, 0, 0, [byte_size_2_float_size(size)], dtype=np.float32, order='C', wait_for=None, is_blocking=True)
		self.host_buf = host_buf
		self.from_buf = cl.Buffer(ctx, 0, size)
		self.to_buf = cl.Buffer(ctx, 0, size)
		self.queueFrom = queueFrom
		self.queueTo = queueTo
		self.size = size

	def __call__(self, waitEvent = None):
		wait_events = [waitEvent] if waitEvent else []
		to_host_event = cl.enqueue_copy(self.queueFrom, self.host_buf, self.from_buf, is_blocking=False, wait_for=wait_events)
		to_dev_event = cl.enqueue_copy(self.queueTo, self.to_buf, self.host_buf, is_blocking=False, wait_for=[to_host_event])
		queueFrom.flush()
		queueTo.flush()
		return to_dev_event

class PIO(Verifyable):
	def __init__(self, size, ctx, queueFrom, queueTo):
		CL_MEM_USE_PERSISTENT_MEM_AMD = 0x40
		self.from_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, size)
		self.to_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, size)
		(self.mapped_from, event) = cl.enqueue_map_buffer(queueFrom, self.from_buf, cl.map_flags.READ, 0, [byte_size_2_float_size(size)], dtype=np.float32, order='C', wait_for=None, is_blocking=True)
		(self.mapped_to, event) = cl.enqueue_map_buffer(queueTo, self.to_buf, cl.map_flags.WRITE, 0, [byte_size_2_float_size(size)], dtype=np.float32, order='C', wait_for=None, is_blocking=True)
		self.queueFrom = queueFrom
		self.queueTo = queueTo
		self.size = size
		self.dummy_event = cl.UserEvent(ctx)
		self.dummy_event.set_status(cl.command_execution_status.COMPLETE)

	def __call__(self, waitEvent = None):
		if waitEvent:
			waitEvent.wait()
		#np.copyto(self.to_buf, self.from_buf)
		self.mapped_to[:] = self.mapped_from
		return self.dummy_event

class PIO_save(Verifyable):
	def __init__(self, size, ctx, queueFrom, queueTo):
		CL_MEM_USE_PERSISTENT_MEM_AMD = 0x40
		self.from_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, size)
		self.to_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, size)
		self.queueFrom = queueFrom
		self.queueTo = queueTo
		self.size = size

	def __call__(self, waitEvent = None):
		waitEvents = [waitEvent] if waitEvent else []

		(mapped_from, mapped_from_event) = cl.enqueue_map_buffer(queueFrom, self.from_buf, cl.map_flags.READ, 0, [byte_size_2_float_size(self.size)], dtype=np.float32, order='C', wait_for=waitEvents, is_blocking=False)
		(mapped_to, mapped_to_event) = cl.enqueue_map_buffer(queueTo, self.to_buf, cl.map_flags.WRITE, 0, [byte_size_2_float_size(self.size)], dtype=np.float32, order='C', wait_for=waitEvents, is_blocking=False)
		cl.wait_for_events([mapped_from_event, mapped_to_event])

		#np.copyto(self.to_buf, self.from_buf)
		mapped_to[:] = mapped_from

		mapped_from.base.release(queueFrom, None)
		return mapped_to.base.release(queueTo, None)

class ReadMapped(Verifyable):
	def __init__(self, size, ctx, queueFrom, queueTo):
		CL_MEM_USE_PERSISTENT_MEM_AMD = 0x40
		self.from_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, size)
		self.to_buf = cl.Buffer(ctx, 0, size)
		(self.mapped_from, event) = cl.enqueue_map_buffer(queueFrom, self.from_buf, cl.map_flags.READ, 0, [byte_size_2_float_size(size)], dtype=np.float32, order='C', wait_for=None, is_blocking=True)
		self.queueFrom = queueFrom
		self.queueTo = queueTo
		self.size = size
		self.dummy_event = cl.UserEvent(ctx)
		self.dummy_event.set_status(cl.command_execution_status.COMPLETE)

	def __call__(self, waitEvent = None):
		if waitEvent:
			waitEvent.wait()
		return cl.enqueue_copy(self.queueTo, self.to_buf, self.mapped_from, is_blocking=False)

class ReadMapped_save(Verifyable):
	def __init__(self, size, ctx, queueFrom, queueTo):
		CL_MEM_USE_PERSISTENT_MEM_AMD = 0x40
		self.from_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, size)
		self.to_buf = cl.Buffer(ctx, 0, size)
		self.queueFrom = queueFrom
		self.queueTo = queueTo
		self.size = size
		self.dummy_event = cl.UserEvent(ctx)
		self.dummy_event.set_status(cl.command_execution_status.COMPLETE)

	def __call__(self, waitEvent = None):
		waitEvents = [waitEvent] if waitEvent else []
		(mapped_from, mapped_from_event) = cl.enqueue_map_buffer(queueFrom, self.from_buf, cl.map_flags.READ, 0, [byte_size_2_float_size(self.size)], dtype=np.float32, order='C', wait_for=waitEvents, is_blocking=False)
		queueFrom.flush()
		write_event = cl.enqueue_copy(self.queueTo, self.to_buf, mapped_from, is_blocking=False, wait_for=[mapped_from_event])
		queueTo.flush()
		return mapped_from.base.release(queueFrom, wait_for=[write_event])

class ReadMapped_saveSynced(Verifyable):
	def __init__(self, size, ctx, queueFrom, queueTo):
		CL_MEM_USE_PERSISTENT_MEM_AMD = 0x40
		self.from_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, size)
		self.to_buf = cl.Buffer(ctx, 0, size)
		self.queueFrom = queueFrom
		self.queueTo = queueTo
		self.size = size
		self.dummy_event = cl.UserEvent(ctx)
		self.dummy_event.set_status(cl.command_execution_status.COMPLETE)

	def __call__(self, waitEvent = None):
		if waitEvent:
			waitEvent.wait()
		(mapped_from, mapped_from_event) = cl.enqueue_map_buffer(queueFrom, self.from_buf, cl.map_flags.READ, 0, [byte_size_2_float_size(self.size)], dtype=np.float32, order='C', wait_for=None, is_blocking=True)
		cl.enqueue_copy(self.queueTo, self.to_buf, mapped_from, is_blocking=True, wait_for=None)
		return mapped_from.base.release(queueFrom, None)

class WriteMapped(Verifyable):
	def __init__(self, size, ctx, queueFrom, queueTo):
		CL_MEM_USE_PERSISTENT_MEM_AMD = 0x40
		self.from_buf = cl.Buffer(ctx, 0, size)
		self.to_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, size)
		(self.mapped_to, event) = cl.enqueue_map_buffer(queueFrom, self.to_buf, cl.map_flags.WRITE, 0, [byte_size_2_float_size(size)], dtype=np.float32, order='C', wait_for=None, is_blocking=True)
		self.queueFrom = queueFrom
		self.queueTo = queueTo
		self.size = size
		self.dummy_event = cl.UserEvent(ctx)
		self.dummy_event.set_status(cl.command_execution_status.COMPLETE)

	def __call__(self, waitEvent = None):
		if waitEvent:
			waitEvent.wait()
		return cl.enqueue_copy(self.queueFrom, self.mapped_to, self.from_buf, is_blocking=False)

class WriteMapped_save(Verifyable):
	def __init__(self, size, ctx, queueFrom, queueTo):
		CL_MEM_USE_PERSISTENT_MEM_AMD = 0x40
		self.from_buf = cl.Buffer(ctx, 0, size)
		self.to_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, size)
		self.queueFrom = queueFrom
		self.queueTo = queueTo
		self.size = size
		self.dummy_event = cl.UserEvent(ctx)
		self.dummy_event.set_status(cl.command_execution_status.COMPLETE)

	def __call__(self, waitEvent = None):
		waitEvents = [waitEvent] if waitEvent else []
		(mapped_to, mapped_to_event) = cl.enqueue_map_buffer(queueTo, self.to_buf, cl.map_flags.WRITE, 0, [byte_size_2_float_size(self.size)], dtype=np.float32, order='C', wait_for=waitEvents, is_blocking=False)
		queueTo.flush()
		read_event = cl.enqueue_copy(self.queueFrom, mapped_to, self.from_buf, is_blocking=False, wait_for=[mapped_to_event])
		queueFrom.flush()
		return mapped_to.base.release(queueTo, [read_event])

class WriteMapped_saveSynced(Verifyable):
	def __init__(self, size, ctx, queueFrom, queueTo):
		CL_MEM_USE_PERSISTENT_MEM_AMD = 0x40
		self.from_buf = cl.Buffer(ctx, 0, size)
		self.to_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, size)
		self.queueFrom = queueFrom
		self.queueTo = queueTo
		self.size = size
		self.dummy_event = cl.UserEvent(ctx)
		self.dummy_event.set_status(cl.command_execution_status.COMPLETE)

	def __call__(self, waitEvent = None):
		if waitEvent:
			waitEvent.wait()
		(mapped_to, mapped_to_event) = cl.enqueue_map_buffer(queueTo, self.to_buf, cl.map_flags.WRITE, 0, [byte_size_2_float_size(self.size)], dtype=np.float32, order='C', wait_for=None, is_blocking=True)
		read_event = cl.enqueue_copy(self.queueFrom, mapped_to, self.from_buf, is_blocking=True, wait_for=None)
		return mapped_to.base.release(queueTo, None)

class Copy(Verifyable):
	def __init__(self, size, ctx, queueFrom, queueTo):
		self.from_buf = cl.Buffer(ctx, 0, size)
		self.to_buf = cl.Buffer(ctx, 0, size)
		self.queueFrom = queueFrom
		self.queueTo = queueTo
		self.size = size
		# ensure buffers are on their respective device
		self.dummy = dummy.get_kernel(ctx, (queueFrom.device, queueTo.device))
		self.dummy(self.queueFrom, (1,), (1,), self.from_buf)
		self.dummy(self.queueTo, (1,), (1,), self.to_buf)

	def __call__(self, waitEvent = None):
		dummy_event = self.dummy(self.queueFrom, (1,), (1,), self.from_buf, wait_for=(waitEvent,) if waitEvent else None)
		self.queueFrom.flush()
		copy_event = cl.enqueue_copy(self.queueTo, self.to_buf, self.from_buf, wait_for=(dummy_event,))
		self.queueTo.flush()
		return copy_event


def benchmark(implementation):

	last_event = None
	start = time.time()
	for i in xrange(args.num_transfers):
		last_event = implementation(last_event)
	last_event.wait()
	end = time.time()

	avg_host = (end - start) / args.num_transfers
	bw_host = implementation.size / avg_host / 1e9
	print '{0:>20}   {1:>20.0f}   {2:>20.2f}'.format(implementation.size, avg_host * 1e6, bw_host)

def check(implementation):
	FLOATS = byte_size_2_float_size(implementation.size)
	for i in xrange(args.num_transfers):
		reference = np.empty(FLOATS, dtype=np.float32)
		reference[:] = np.random.rand(FLOATS)
		implementation.set_src(reference)
		event = implementation(None)
		event.wait()
		result = implementation.get_dest()
		if (result != reference).any():
			print 'Destination values do not match source values!'
			return
	print 'No errors found.'
	return


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Benchmark PCI bandwidth')
	parser.add_argument('src', type=int, help='The device from which to send the data')
	parser.add_argument('dest', type=int, help='The device to which to send the data')
	parser.add_argument('-p', '--platform', type=int, metavar='I', help='The platform to use for the measurement (requires device to be specified)')
	parser.add_argument('-s', '--size', type=int, metavar='BYTE', help='Memory size in byte', default=4*1024*1024)
	parser.add_argument('-t', '--type', metavar='TYPE', help='The type of memory/transfer to used', default='plain')
	parser.add_argument('-n', '--num-transfers', type=int, help='How many transfers to perform', default=1000)
	parser.add_argument('-c', '--check', action='store_true', default=False, help='Instead of benchmarking check whether destination buffer contains correct data.')
	parser.add_argument('--sweep', action='store_true', default=False, help='Sweep the range of potential memory sizes')

	args = parser.parse_args()

	(ctx, queueFrom, queueTo) = init_cl(args.platform, args.src, args.dest)

	def make_implementation(size):
		if args.type == 'plain':
			return PlainMemoryBlocking(size, ctx, queueFrom, queueTo)
		elif args.type == 'plainAsync':
			return PlainMemoryAsync(size, ctx, queueFrom, queueTo)
		elif args.type == 'pinned':
			return PinnedMemoryBlocking(size, ctx, queueFrom, queueTo)
		elif args.type == 'pinnedAsync':
			return PinnedMemoryAsync(size, ctx, queueFrom, queueTo)
		elif args.type == 'host2host':
			return Host2Host(size, ctx)
		elif args.type == 'pio':
			return PIO(size, ctx, queueFrom, queueTo)
		elif args.type == 'pio_save':
			return PIO_save(size, ctx, queueFrom, queueTo)
		elif args.type == 'readMapped':
			return ReadMapped(size, ctx, queueFrom, queueTo)
		elif args.type == 'readMapped_save':
			return ReadMapped_save(size, ctx, queueFrom, queueTo)
		elif args.type == 'readMapped_saveSynced':
			return ReadMapped_saveSynced(size, ctx, queueFrom, queueTo)
		elif args.type == 'writeMapped':
			return WriteMapped(size, ctx, queueFrom, queueTo)
		elif args.type == 'writeMapped_save':
			return WriteMapped_save(size, ctx, queueFrom, queueTo)
		elif args.type == 'writeMapped_saveSynced':
			return WriteMapped_saveSynced(size, ctx, queueFrom, queueTo)
		elif args.type == 'copy':
			return Copy(size, ctx, queueFrom, queueTo)
		else:
			print 'The memory/transfer type "{0}" does not exist'.format(args.type)
			sys.exit(1)

	if args.check:
		check(implementation, args.num_transfers)
	else:
		print '{0:>20}   {1:>20}   {2:>20}'.format('Buffer Size / B', 'Transfer Time / mus', 'Bandwidth / GB/s')
		if args.sweep:
			from sweep import sweep
			sweep(make_implementation, benchmark)
		else:
			implementation = make_implementation(args.size)
			benchmark(implementation)
