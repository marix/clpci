#!/usr/bin/env python
# coding=utf8

# This file is part of clPCI.
#
# clPCI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# clPCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with clPCI.  If not, see <http://www.gnu.org/licenses/>.
#
# (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>

import pyopencl as cl
import argparse
import numpy as np

CL_MEM_USE_PERSISTENT_MEM_AMD = 0x40

def init_cl(platform, deviceFrom, deviceTo):
	platforms = cl.get_platforms()
	if len(platforms) > 1 and platform == None:
		raise Exception('Found more then one platform, giving up.')
	platform = platforms[platform if platform != None else 0]
	properties = [(cl.context_properties.PLATFORM, platform)]
	platform_devices = platform.get_devices()
	ctx = cl.Context(platform_devices, properties)
	queueFrom = cl.CommandQueue(ctx, platform_devices[deviceFrom])
	queueTo = cl.CommandQueue(ctx, platform_devices[deviceTo])
	return (ctx, queueFrom, queueTo)

def check(ctx, queueFrom, queueTo, size, zeroCopy, num_transfers):
	TYPE=np.float32
	byte_size = size * 4
	mem_flags = CL_MEM_USE_PERSISTENT_MEM_AMD if zeroCopy else cl.mem_flags.ALLOC_HOST_PTR
	from_buf = cl.Buffer(ctx, mem_flags, byte_size)
	to_buf = cl.Buffer(ctx, mem_flags, byte_size)

	zeros = np.zeros(size, dtype=TYPE)
	cl.enqueue_copy(queueFrom, from_buf, zeros)
	cl.enqueue_copy(queueTo, to_buf, zeros)

	for iteration in xrange(num_transfers):
		reference = np.empty(size, dtype=TYPE)
		reference[:] = np.random.rand(size)
		cl.enqueue_copy(queueFrom, from_buf, reference)

		(mapped_from, map_from_event) = cl.enqueue_map_buffer(queueFrom, from_buf, cl.map_flags.READ, 0, reference.shape, dtype=TYPE, order='C', wait_for=None, is_blocking=True)
		(mapped_to, map_to_event) = cl.enqueue_map_buffer(queueTo, to_buf, cl.map_flags.WRITE, 0, reference.shape, dtype=TYPE, order='C', wait_for=None, is_blocking=True)

		if (mapped_from != reference).any():
			print 'Test FAILED: Reading from source gives wrong values.'

		for i in xrange(len(reference)):
			mapped_to[i] = mapped_from[i]
#mapped_to[:] = mapped_from

		if (mapped_to != reference).any():
			print 'Test FAILED: Reading from mapped destination gives wrong values.'

		to_release = mapped_to.base.release(queueTo)
		from_release = mapped_from.base.release(queueFrom)
		cl.wait_for_events([from_release, to_release])

		read_back = np.empty_like(reference)
		cl.enqueue_copy(queueTo, read_back, to_buf)

		if (read_back != reference).any():
			print 'Test FAILED: Destination values do not match source values after unmap!'
			return
	
	print 'Test PASSED'
	return


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Check copying mapped memory')
	parser.add_argument('src', type=int, help='The device from which to send the data')
	parser.add_argument('dest', type=int, help='The device to which to send the data')
	parser.add_argument('-p', '--platform', type=int, metavar='I', help='The platform to use for the measurement (requires device to be specified)')
	parser.add_argument('-s', '--size', type=int, metavar='N', help='Buffer size in floats', default=1024*1024)
	parser.add_argument('-n', '--num-transfers', type=int, help='How many transfers to perform', default=100)
	parser.add_argument('-z', '--zero-copy', action='store_true', default=False, help='Use host visible device memory')

	args = parser.parse_args()

	(ctx, queueFrom, queueTo) = init_cl(args.platform, args.src, args.dest)

	check(ctx, queueFrom, queueTo, args.size, args.zero_copy, args.num_transfers)
