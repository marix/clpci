#!/usr/bin/env python
# coding=utf8

# This file is part of clPCI.
#
# clPCI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# clPCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with clPCI.  If not, see <http://www.gnu.org/licenses/>.
#
# (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>

import pyopencl as cl
import sys

def sweep(make_implementation, run_benchmark):
	for size in (2**i for i in xrange(30)):
		try:
			implementation = make_implementation(size)
			run_benchmark(implementation)
		except cl.LogicError as e:
			sys.stderr.write('Failed for size of {0} B: {1}\n'.format(size, e))
			pass
