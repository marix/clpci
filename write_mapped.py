#!/usr/bin/env python
# coding=utf8

# This file is part of clPCI.
#
# clPCI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# clPCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with clPCI.  If not, see <http://www.gnu.org/licenses/>.
#
# (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>

import pyopencl as cl
import argparse
import numpy as np

CL_MEM_USE_PERSISTENT_MEM_AMD = 0x40

def init_cl(platform, devices):
	if len(devices) > 0:
		platforms = cl.get_platforms()
		if len(platforms) > 1 and platform == None:
			raise Exception('Found more then one platform, giving up.')
		platform = platforms[platform if platform != None else 0]
		properties = [(cl.context_properties.PLATFORM, platform)]
		cl_devices = [platform.get_devices()[device] for device in devices]
		ctx = cl.Context(cl_devices, properties)
		queues = [cl.CommandQueue(ctx, device) for device in cl_devices]
	else:
		ctx = cl.create_some_context()
		queues = [cl.CommandQueue(ctx)]
	return (ctx, queues)

def check(ctx, queues, size, zeroCopy, num_transfers, num_buffers):
	TYPE=np.float32
	byte_size = size * 4
	mem_flags = CL_MEM_USE_PERSISTENT_MEM_AMD if zeroCopy else cl.mem_flags.ALLOC_HOST_PTR
	queue_bufs = [[cl.Buffer(ctx, mem_flags, byte_size) for i in xrange(num_buffers)] for queue in queues]

	zeros = np.zeros(size, dtype=TYPE)
	for (queue, dev_bufs) in zip(queues, queue_bufs):
		for dev_buf in dev_bufs:
			cl.enqueue_copy(queue, dev_buf, zeros)
	for (queue, dev_bufs) in zip(queues, queue_bufs):
		for dev_buf in dev_bufs:
				read_back = np.empty_like(zeros)
				cl.enqueue_copy(queue, read_back, dev_buf)

				if (read_back != zeros).any():
					print 'Test FAILED: Initialization failed!'
					return

	for iteration in xrange(num_transfers):
		reference = np.empty(size, dtype=TYPE)
		reference[:] = np.random.rand(size)

		maps = [cl.enqueue_map_buffer(queue, dev_buf, cl.map_flags.WRITE, 0, reference.shape, dtype=TYPE, order='C', wait_for=None, is_blocking=True) + (queue,) for (queue, dev_bufs) in zip(queues, queue_bufs) for dev_buf in dev_bufs]

		for (mapped_buf, mapped_event, queue) in maps:
			mapped_buf[:] = reference

		for (mapped_buf, mapped_event, queue) in maps:
			release_event = mapped_buf.base.release(queue)

		for (queue, dev_bufs) in zip(queues, queue_bufs):
			for dev_buf in dev_bufs:
				read_back = np.empty_like(reference)
				cl.enqueue_copy(queue, read_back, dev_buf)

				if (read_back != reference).any():
					print 'Test FAILED: Destination values do not match source values!'
					return

	print 'Test PASSED'
	return


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Check writing to mapped memory')
	parser.add_argument('-d', '--device', nargs='*', type=int, metavar='I', help='The device to use for the measurement')
	parser.add_argument('-p', '--platform', type=int, metavar='I', help='The platform to use for the measurement (requires device to be specified)')
	parser.add_argument('-s', '--size', type=int, metavar='N', help='Buffer size in floats', default=1024*1024)
	parser.add_argument('-n', '--num-transfers', type=int, help='How many transfers to perform', default=100)
	parser.add_argument('-z', '--zero-copy', action='store_true', default=False, help='Use host visible device memory')
	parser.add_argument('-b', '--num-buffers', type=int, help='How many buffers to use', default=1)

	args = parser.parse_args()

	(ctx, queue) = init_cl(args.platform, args.device)

	check(ctx, queue, args.size, args.zero_copy, args.num_transfers, args.num_buffers)
