#!/usr/bin/env python
# coding=utf8

# This file is part of clPCI.
#
# clPCI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# clPCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with clPCI.  If not, see <http://www.gnu.org/licenses/>.
#
# (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>

import pyopencl as cl
import argparse
import sys
import numpy as np
import math
import time

def init_cl(platform, device):
	if device != None:
		platforms = cl.get_platforms()
		if len(platforms) > 1 and platform == None:
			raise Exception('Found more then one platform, giving up.')
		platform = platforms[platform if platform != None else 0]
		properties = [(cl.context_properties.PLATFORM, platform)]
		devices = [platform.get_devices()[device]]
		ctx = cl.Context(devices, properties)
	else:
		ctx = cl.create_some_context()
	queue = cl.CommandQueue(ctx)
	return (ctx, queue)

def byte_size_2_float_size(floats):
	return floats / 4


class PlainMemoryBlocking(object):
	def __init__(self, size, ctx, queue):
		self.host_buf = np.empty(byte_size_2_float_size(size), dtype=np.float32)
		self.dev_buf = cl.Buffer(ctx, 0, size)
		self.queue = queue
		self.size = size

	def __call__(self, waitEvent = None):
		if waitEvent:
			waitEvent.wait()
		cl.enqueue_copy(self.queue, self.host_buf, self.dev_buf, is_blocking=True)
		return cl.enqueue_copy(self.queue, self.dev_buf, self.host_buf, is_blocking=True)

class PlainMemoryAsync(object):
	def __init__(self, size, ctx, queue):
		self.host_buf = np.empty(byte_size_2_float_size(size), dtype=np.float32)
		self.dev_buf = cl.Buffer(ctx, 0, size)
		self.queue = queue
		self.size = size

	def __call__(self, waitEvent = None):
		wait_events = [waitEvent] if waitEvent else []
		dev_host_event = cl.enqueue_copy(self.queue, self.host_buf, self.dev_buf, is_blocking=False, wait_for=wait_events)
		dev_dev_event = cl.enqueue_copy(self.queue, self.dev_buf, self.host_buf, is_blocking=False, wait_for=[dev_host_event])
		queue.flush()
		return dev_dev_event

class PinnedMemoryBlocking(object):
	def __init__(self, size, ctx, queue):
		self.pin_buf = cl.Buffer(ctx, cl.mem_flags.ALLOC_HOST_PTR, size)
		(host_buf, event) = cl.enqueue_map_buffer(queue, self.pin_buf, 0, 0, [byte_size_2_float_size(size)], dtype=np.float32, order='C', wait_for=None, is_blocking=True)
		self.host_buf = host_buf
		self.dev_buf = cl.Buffer(ctx, 0, size)
		self.queue = queue
		self.size = size

	def __call__(self, waitEvent = None):
		if waitEvent:
			waitEvent.wait()
		cl.enqueue_copy(self.queue, self.host_buf, self.dev_buf, is_blocking=True)
		return cl.enqueue_copy(self.queue, self.dev_buf, self.host_buf, is_blocking=True)

class PinnedMemoryAsync(object):
	def __init__(self, size, ctx, queue):
		self.pin_buf = cl.Buffer(ctx, cl.mem_flags.ALLOC_HOST_PTR, size)
		(host_buf, event) = cl.enqueue_map_buffer(queue, self.pin_buf, 0, 0, [byte_size_2_float_size(size)], dtype=np.float32, order='C', wait_for=None, is_blocking=True)
		self.host_buf = host_buf
		self.dev_buf = cl.Buffer(ctx, 0, size)
		self.queue = queue
		self.size = size

	def __call__(self, waitEvent = None):
		wait_events = [waitEvent] if waitEvent else []
		to_host_event = cl.enqueue_copy(self.queue, self.host_buf, self.dev_buf, is_blocking=False, wait_for=wait_events)
		to_dev_event = cl.enqueue_copy(self.queue, self.host_buf, self.dev_buf, is_blocking=False, wait_for=[to_host_event])
		queue.flush()
		return to_dev_event

def benchmark(implementation):

	last_event = None
	start = time.time()
	for i in xrange(args.num_transfers):
		last_event = implementation(last_event)
	last_event.wait()
	end = time.time()

	avg_host = (end - start) / args.num_transfers
	bw_host = implementation.size / avg_host / 1e9

	print '{0:>20}   {1:>20.0f}   {2:>20.2f}'.format(implementation.size, avg_host * 1e6, bw_host)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Benchmark PCI bandwidth')
	parser.add_argument('-d', '--device', type=int, metavar='I', help='The device to use for the measurement')
	parser.add_argument('-p', '--platform', type=int, metavar='I', help='The platform to use for the measurement (requires device to be specified)')
	parser.add_argument('-s', '--size', type=int, metavar='BYTE', help='Memory size in byte', default=4*1024*1024)
	parser.add_argument('-t', '--type', metavar='TYPE', help='The type of memory/transfer to used', default='plain')
	parser.add_argument('-n', '--num-transfers', type=int, help='How many transfers to perform', default=1000)
	parser.add_argument('--sweep', action='store_true', default=False, help='Sweep the range of potential memory sizes')

	args = parser.parse_args()

	(ctx, queue) = init_cl(args.platform, args.device)

	def make_implementation(size):
		if args.type == 'plain':
			return PlainMemoryBlocking(size, ctx, queue)
		elif args.type == 'plainAsync':
			return PlainMemoryAsync(size, ctx, queue)
		elif args.type == 'pinned':
			return PinnedMemoryBlocking(size, ctx, queue)
		elif args.type == 'pinnedAsync':
			return PinnedMemoryAsync(size, ctx, queue)
		else:
			print 'The memory/transfer type "{0}" does not exist'.format(args.type)
			sys.exit(1)

	print '{0:>20}   {1:>20}   {2:>20}'.format('Buffer Size / B', 'Transfer Time / mus', 'Bandwidth / GB/s')
	if args.sweep:
		from sweep import sweep
		sweep(make_implementation, benchmark)
	else:
		implementation = make_implementation(args.size)
		benchmark(implementation)
