#!/usr/bin/env python
# coding=utf8

# This file is part of clPCI.
#
# clPCI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# clPCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with clPCI.  If not, see <http://www.gnu.org/licenses/>.
#
# (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>

import pyopencl as cl
import argparse
import sys
import numpy as np
import math
import time
from threading import Thread
import dummy

def init_cl(platform, device1, device2):
	platforms = cl.get_platforms()
	if len(platforms) > 1 and platform == None:
		raise Exception('Found more then one platform, giving up.')
	platform = platforms[platform if platform != None else 0]
	properties = [(cl.context_properties.PLATFORM, platform)]
	platform_devices = platform.get_devices()
	ctx = cl.Context(platform_devices, properties)
	queue1 = cl.CommandQueue(ctx, platform_devices[device1])
	queue2 = cl.CommandQueue(ctx, platform_devices[device2])
	return (ctx, queue1, queue2)

def byte_size_2_float_size(floats):
	return floats / 4

def float_size_2_byte_size(bytes):
	return bytes * 4

class Verifyable(object):
	def set_src(self, dev1_val, dev2_val):
		cl.enqueue_copy(self.dev1_queue, self.dev1_buf, dev1_val, is_blocking=True)
		cl.enqueue_copy(self.dev2_queue, self.dev2_buf, dev2_val, is_blocking=True)
	def get_contents(self):
		dev1_res = np.empty(self.elem_size, dtype=np.float32)
		dev2_res = np.empty(self.elem_size, dtype=np.float32)
		cl.enqueue_copy(self.dev1_queue, dev1_res, self.dev1_buf, is_blocking=True)
		cl.enqueue_copy(self.dev2_queue, dev2_res, self.dev2_buf, is_blocking=True)
		return (dev1_res, dev2_res)

class PlainMemoryBlocking(Verifyable):
	def __init__(self, byte_size, ctx, queue1, queue2):
		self.byte_size = byte_size
		self.elem_size = byte_size_2_float_size(byte_size)
		self.host_buf = np.empty(self.elem_size, dtype=np.float32)
		self.dev1_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev2_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev1_queue = queue1
		self.dev2_queue = queue2

	def __call__(self, waitEvents = None):
		if waitEvents:
			cl.wait_for_events(waitEvents)
		sentinel = self.elem_size / 2
		sentinel_bytes = float_size_2_byte_size(sentinel)
		cl.enqueue_copy(self.dev1_queue, self.host_buf[:sentinel], self.dev1_buf, is_blocking=True)
		cl.enqueue_copy(self.dev2_queue, self.host_buf[sentinel:], self.dev2_buf, device_offset=sentinel_bytes, is_blocking=True)

		cl.enqueue_copy(self.dev1_queue, self.dev1_buf, self.host_buf[sentinel:], device_offset=sentinel_bytes, is_blocking=True)
		dev2_event = cl.enqueue_copy(self.dev2_queue, self.dev2_buf, self.host_buf[:sentinel], is_blocking=True)
		return [dev2_event]

class PlainMemory(Verifyable):
	def __init__(self, byte_size, ctx, queue1, queue2):
		self.byte_size = byte_size
		self.elem_size = byte_size_2_float_size(byte_size)
		self.host_buf = np.empty(self.elem_size, dtype=np.float32)
		self.dev1_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev2_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev1_queue = queue1
		self.dev2_queue = queue2

	def __call__(self, waitEvents = None):
		sentinel = self.elem_size / 2
		sentinel_bytes = float_size_2_byte_size(sentinel)

		dev1_toHost = cl.enqueue_copy(self.dev1_queue, self.host_buf[:sentinel], self.dev1_buf, is_blocking=False, wait_for=waitEvents)
		dev2_toHost = cl.enqueue_copy(self.dev2_queue, self.host_buf[sentinel:], self.dev2_buf, device_offset=sentinel_bytes, wait_for=waitEvents)

		dev1_toDev = cl.enqueue_copy(self.dev1_queue, self.dev1_buf, self.host_buf[sentinel:], device_offset=sentinel_bytes, is_blocking=False, wait_for=[dev2_toHost])
		dev2_toDev = cl.enqueue_copy(self.dev2_queue, self.dev2_buf, self.host_buf[:sentinel], is_blocking=False, wait_for=[dev1_toHost])
		return [dev1_toDev, dev2_toDev]

class PinnedBlocking(Verifyable):
	def __init__(self, byte_size, ctx, queue1, queue2):
		self.byte_size = byte_size
		self.elem_size = byte_size_2_float_size(byte_size)
		self.pin_buf = cl.Buffer(ctx, cl.mem_flags.ALLOC_HOST_PTR, byte_size)
		(self.host_buf, _) = cl.enqueue_map_buffer(queue1, self.pin_buf, 0, 0, [self.elem_size], dtype=np.float32, order='C', wait_for=None, is_blocking=True)
		self.dev1_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev2_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev1_queue = queue1
		self.dev2_queue = queue2

	def __call__(self, waitEvents = None):
		if waitEvents:
			cl.wait_for_events(waitEvents)
		sentinel = self.elem_size / 2
		sentinel_bytes = float_size_2_byte_size(sentinel)
		cl.enqueue_copy(self.dev1_queue, self.host_buf[:sentinel], self.dev1_buf, is_blocking=True)
		cl.enqueue_copy(self.dev2_queue, self.host_buf[sentinel:], self.dev2_buf, device_offset=sentinel_bytes, is_blocking=True)

		cl.enqueue_copy(self.dev1_queue, self.dev1_buf, self.host_buf[sentinel:], device_offset=sentinel_bytes, is_blocking=True)
		dev2_event = cl.enqueue_copy(self.dev2_queue, self.dev2_buf, self.host_buf[:sentinel], is_blocking=True)
		return [dev2_event]

class Pinned(Verifyable):
	def __init__(self, byte_size, ctx, queue1, queue2):
		self.byte_size = byte_size
		self.elem_size = byte_size_2_float_size(byte_size)
		self.pin_buf = cl.Buffer(ctx, cl.mem_flags.ALLOC_HOST_PTR, byte_size)
		(self.host_buf, _) = cl.enqueue_map_buffer(queue1, self.pin_buf, 0, 0, [self.elem_size], dtype=np.float32, order='C', wait_for=None, is_blocking=True)
		self.dev1_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev2_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev1_queue = queue1
		self.dev2_queue = queue2

	def __call__(self, waitEvents = None):
		sentinel = self.elem_size / 2
		sentinel_bytes = float_size_2_byte_size(sentinel)

		dev1_toHost = cl.enqueue_copy(self.dev1_queue, self.host_buf[:sentinel], self.dev1_buf, is_blocking=False, wait_for=waitEvents)
		dev2_toHost = cl.enqueue_copy(self.dev2_queue, self.host_buf[sentinel:], self.dev2_buf, device_offset=sentinel_bytes, wait_for=waitEvents)

		dev1_toDev = cl.enqueue_copy(self.dev1_queue, self.dev1_buf, self.host_buf[sentinel:], device_offset=sentinel_bytes, is_blocking=False, wait_for=[dev2_toHost])
		dev2_toDev = cl.enqueue_copy(self.dev2_queue, self.dev2_buf, self.host_buf[:sentinel], is_blocking=False, wait_for=[dev1_toHost])
		return [dev1_toDev, dev2_toDev]

class PinnedMultiHostBuf(Verifyable):
	def __init__(self, byte_size, ctx, queue1, queue2):
		self.byte_size = byte_size
		self.elem_size = byte_size_2_float_size(byte_size)
		self.pin_bufs = [cl.Buffer(ctx, cl.mem_flags.ALLOC_HOST_PTR, byte_size / 2) for _ in xrange(2)]
		self.host_bufs = [cl.enqueue_map_buffer(queue, pin_buf, 0, 0, [self.elem_size / 2], dtype=np.float32, order='C', wait_for=None, is_blocking=True)[0] for (queue, pin_buf) in zip([queue1, queue2], self.pin_bufs)]
		self.dev1_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev2_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev1_queue = queue1
		self.dev2_queue = queue2

	def __call__(self, waitEvents = None):
		sentinel = self.elem_size / 2
		sentinel_bytes = float_size_2_byte_size(sentinel)

		dev1_toHost = cl.enqueue_copy(self.dev1_queue, self.host_bufs[0], self.dev1_buf, is_blocking=False, wait_for=waitEvents)
		dev2_toHost = cl.enqueue_copy(self.dev2_queue, self.host_bufs[1], self.dev2_buf, device_offset=sentinel_bytes, wait_for=waitEvents)

		dev1_toDev = cl.enqueue_copy(self.dev1_queue, self.dev1_buf, self.host_bufs[1], device_offset=sentinel_bytes, is_blocking=False, wait_for=[dev2_toHost])
		dev2_toDev = cl.enqueue_copy(self.dev2_queue, self.dev2_buf, self.host_bufs[0], is_blocking=False, wait_for=[dev1_toHost])
		return [dev1_toDev, dev2_toDev]

class PIO(Verifyable):
	def __init__(self, byte_size, ctx, queue1, queue2):
		self.byte_size = byte_size
		self.elem_size = byte_size_2_float_size(byte_size)
		CL_MEM_USE_PERSISTENT_MEM_AMD = 0x40
		self.dev1_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, byte_size)
		self.dev2_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, byte_size)
		self.dev1_queue = queue1
		self.dev2_queue = queue2

	def __call__(self, waitEvents = None):
		sentinel = self.elem_size / 2

		(dev1_map, dev1_map_event) = cl.enqueue_map_buffer(queue1, self.dev1_buf, cl.map_flags.READ | cl.map_flags.WRITE, 0, [self.elem_size], dtype=np.float32, order='C', wait_for=waitEvents, is_blocking=False)
		(dev2_map, dev2_map_event) = cl.enqueue_map_buffer(queue2, self.dev2_buf, cl.map_flags.READ | cl.map_flags.WRITE, 0, [self.elem_size], dtype=np.float32, order='C', wait_for=waitEvents, is_blocking=False)
		cl.wait_for_events((dev1_map_event, dev2_map_event))

		dev1_map[sentinel:] = dev2_map[sentinel:]
		dev2_map[:sentinel] = dev1_map[:sentinel]

		return [dev1_map.base.release(queue1), dev2_map.base.release(queue2)]

class CopyThread(Thread):
	def __init__(self, dest, src):
		Thread.__init__(self)
		self.dest = dest
		self.src = src
	def run(self):
		self.dest[:] = self.src

class PIO_threaded(Verifyable):
	def __init__(self, byte_size, ctx, queue1, queue2):
		self.byte_size = byte_size
		self.elem_size = byte_size_2_float_size(byte_size)
		CL_MEM_USE_PERSISTENT_MEM_AMD = 0x40
		self.dev1_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, byte_size)
		self.dev2_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, byte_size)
		self.dev1_queue = queue1
		self.dev2_queue = queue2

	def __call__(self, waitEvents = None):
		sentinel = self.elem_size / 2

		(dev1_map, dev1_map_event) = cl.enqueue_map_buffer(queue1, self.dev1_buf, cl.map_flags.READ | cl.map_flags.WRITE, 0, [self.elem_size], dtype=np.float32, order='C', wait_for=waitEvents, is_blocking=False)
		(dev2_map, dev2_map_event) = cl.enqueue_map_buffer(queue2, self.dev2_buf, cl.map_flags.READ | cl.map_flags.WRITE, 0, [self.elem_size], dtype=np.float32, order='C', wait_for=waitEvents, is_blocking=False)
		cl.wait_for_events((dev1_map_event, dev2_map_event))

		t1 = CopyThread(dev1_map[sentinel:], dev2_map[sentinel:])
#		t2 = CopyThread(dev2_map[:sentinel], dev1_map[:sentinel])
		t1.start()
		dev2_map[:sentinel] = dev1_map[:sentinel]
#		t2.start()
		t1.join()
#		t2.join()

		return [dev1_map.base.release(queue1), dev2_map.base.release(queue2)]

class ReadMapped(Verifyable):
	def __init__(self, byte_size, ctx, queue1, queue2):
		self.byte_size = byte_size
		self.elem_size = byte_size_2_float_size(byte_size)
		CL_MEM_USE_PERSISTENT_MEM_AMD = 0x40
		self.dev1_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, byte_size)
		self.dev2_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, byte_size)
		self.dev1_queue = queue1
		self.dev2_queue = queue2

	def __call__(self, waitEvents = None):
		sentinel = self.elem_size / 2
		sentinel_bytes = float_size_2_byte_size(sentinel)

		# map half of each side
		(dev1_map, dev1_map_event) = cl.enqueue_map_buffer(self.dev1_queue, self.dev1_buf, cl.map_flags.READ, 0, [sentinel], dtype=np.float32, order='C', wait_for=waitEvents, is_blocking=False)
		(dev2_map, dev2_map_event) = cl.enqueue_map_buffer(self.dev2_queue, self.dev2_buf, cl.map_flags.READ , sentinel_bytes, [sentinel], dtype=np.float32, order='C', wait_for=waitEvents, is_blocking=False)

		dev1_read = cl.enqueue_copy(self.dev1_queue, self.dev1_buf, dev2_map, device_offset=sentinel_bytes, is_blocking=False, wait_for=(dev2_map_event,))
		dev2_read = cl.enqueue_copy(self.dev2_queue, self.dev2_buf, dev1_map, device_offset=0, is_blocking=False, wait_for=(dev1_map_event,))

		return (dev1_map.base.release(self.dev1_queue, wait_for=(dev2_read,)), dev2_map.base.release(self.dev2_queue, wait_for=(dev1_read,)))

class WriteMapped(Verifyable):
	def __init__(self, byte_size, ctx, queue1, queue2):
		self.byte_size = byte_size
		self.elem_size = byte_size_2_float_size(byte_size)
		CL_MEM_USE_PERSISTENT_MEM_AMD = 0x40
		self.dev1_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, byte_size)
		self.dev2_buf = cl.Buffer(ctx, CL_MEM_USE_PERSISTENT_MEM_AMD, byte_size)
		self.dev1_queue = queue1
		self.dev2_queue = queue2

	def __call__(self, waitEvents = None):
		sentinel = self.elem_size / 2
		sentinel_bytes = float_size_2_byte_size(sentinel)

		# map half of each side
		(dev1_map, dev1_map_event) = cl.enqueue_map_buffer(self.dev1_queue, self.dev1_buf, cl.map_flags.WRITE, sentinel_bytes, [sentinel], dtype=np.float32, order='C', wait_for=waitEvents, is_blocking=False)
		(dev2_map, dev2_map_event) = cl.enqueue_map_buffer(self.dev2_queue, self.dev2_buf, cl.map_flags.WRITE, 0, [sentinel], dtype=np.float32, order='C', wait_for=waitEvents, is_blocking=False)

		dev1_write = cl.enqueue_copy(self.dev1_queue, dev2_map, self.dev1_buf, device_offset=0, is_blocking=False, wait_for=(dev2_map_event,))
		dev2_write = cl.enqueue_copy(self.dev2_queue, dev1_map, self.dev2_buf, device_offset=sentinel_bytes, is_blocking=False, wait_for=(dev1_map_event,))

		return (dev1_map.base.release(self.dev1_queue, wait_for=(dev2_write,)), dev2_map.base.release(self.dev2_queue, wait_for=(dev1_write,)))

class CopyFrom(Verifyable):
	def __init__(self, byte_size, ctx, queue1, queue2):
		self.byte_size = byte_size
		self.elem_size = byte_size_2_float_size(byte_size)
		self.dev1_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev2_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev1_queue = queue1
		self.dev2_queue = queue2

	def __call__(self, waitEvents = None):
		sentinel = self.elem_size / 2
		sentinel_bytes = float_size_2_byte_size(sentinel)
		dev1_copy = cl.enqueue_copy(self.dev1_queue, self.dev1_buf, self.dev2_buf, dest_offset=sentinel_bytes, src_offset=sentinel_bytes, byte_count=sentinel_bytes, wait_for=waitEvents)
		dev2_copy = cl.enqueue_copy(self.dev2_queue, self.dev2_buf, self.dev1_buf, dest_offset=0, src_offset=0, byte_count=sentinel_bytes, wait_for=waitEvents)
		return (dev1_copy, dev2_copy)

class CopyFrom_save(Verifyable):
	def __init__(self, byte_size, ctx, queue1, queue2):
		self.byte_size = byte_size
		self.elem_size = byte_size_2_float_size(byte_size)
		self.dev1_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev2_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev1_queue = queue1
		self.dev2_queue = queue2
		self.dummy = dummy.get_kernel(ctx, (self.dev1_queue.device, self.dev2_queue.device))

	def __call__(self, waitEvents = None):
		sentinel = self.elem_size / 2
		sentinel_bytes = float_size_2_byte_size(sentinel)
		dev1_copy = cl.enqueue_copy(self.dev1_queue, self.dev1_buf, self.dev2_buf, dest_offset=sentinel_bytes, src_offset=sentinel_bytes, byte_count=sentinel_bytes, wait_for=waitEvents)
		dev2_copy = cl.enqueue_copy(self.dev2_queue, self.dev2_buf, self.dev1_buf, dest_offset=0, src_offset=0, byte_count=sentinel_bytes, wait_for=waitEvents)
		dev1_save = self.dummy(self.dev1_queue, (1,), (1,), self.dev1_buf, wait_for=(dev1_copy, dev2_copy))
		dev2_save = self.dummy(self.dev2_queue, (1,), (1,), self.dev2_buf, wait_for=(dev1_copy, dev2_copy))
		return (dev1_save, dev2_save)

class CopyFrom_proxied(Verifyable):
	def __init__(self, byte_size, ctx, queue1, queue2):
		self.byte_size = byte_size
		self.elem_size = byte_size_2_float_size(byte_size)
		self.dev1_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev2_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev1_proxy = cl.Buffer(ctx, 0, byte_size / 2)
		self.dev2_proxy = cl.Buffer(ctx, 0, byte_size / 2)
		self.dev1_queue = queue1
		self.dev2_queue = queue2

	def __call__(self, waitEvents = None):
		sentinel = self.elem_size / 2
		sentinel_bytes = float_size_2_byte_size(sentinel)
		dev1_proxy_filled = cl.enqueue_copy(self.dev1_queue, self.dev1_proxy, self.dev1_buf, dest_offset=0, src_offset=0, byte_count=sentinel_bytes, wait_for=waitEvents)
		dev2_proxy_filled = cl.enqueue_copy(self.dev2_queue, self.dev2_proxy, self.dev2_buf, dest_offset=0, src_offset=sentinel_bytes, byte_count=sentinel_bytes, wait_for=waitEvents)
		dev1_filled = cl.enqueue_copy(self.dev1_queue, self.dev1_buf, self.dev2_proxy, dest_offset=sentinel_bytes, src_offset=0, byte_count=sentinel_bytes, wait_for=(dev2_proxy_filled,))
		dev2_filled = cl.enqueue_copy(self.dev2_queue, self.dev2_buf, self.dev1_proxy, dest_offset=0, src_offset=0, byte_count=sentinel_bytes, wait_for=(dev1_proxy_filled,))
		return (dev1_filled, dev2_filled)

class CopyFrom_proxied12(Verifyable):
	def __init__(self, byte_size, ctx, queue1, queue2):
		self.byte_size = byte_size
		self.elem_size = byte_size_2_float_size(byte_size)
		self.dev1_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev2_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev1_proxy = cl.Buffer(ctx, 0, byte_size / 2)
		self.dev2_proxy = cl.Buffer(ctx, 0, byte_size / 2)
		self.dev1_queue = queue1
		self.dev2_queue = queue2

	def __call__(self, waitEvents = None):
		sentinel = self.elem_size / 2
		sentinel_bytes = float_size_2_byte_size(sentinel)
		dev1_proxy_filled = cl.enqueue_copy(self.dev1_queue, self.dev1_proxy, self.dev1_buf, dest_offset=0, src_offset=0, byte_count=sentinel_bytes, wait_for=waitEvents)
		dev2_proxy_filled = cl.enqueue_copy(self.dev2_queue, self.dev2_proxy, self.dev2_buf, dest_offset=0, src_offset=sentinel_bytes, byte_count=sentinel_bytes, wait_for=waitEvents)
		dev1_filled = cl.enqueue_copy(self.dev1_queue, self.dev1_buf, self.dev2_proxy, dest_offset=sentinel_bytes, src_offset=0, byte_count=sentinel_bytes, wait_for=(dev2_proxy_filled,))
		dev2_filled = cl.enqueue_copy(self.dev2_queue, self.dev2_buf, self.dev1_proxy, dest_offset=0, src_offset=0, byte_count=sentinel_bytes, wait_for=(dev1_proxy_filled,))
		# send back proxy objects without contents
		cl.enqueue_migrate_mem_objects(self.dev1_queue, (self.dev1_proxy,), flags=cl.mem_migration_flags.CONTENT_UNDEFINED, wait_for=(dev2_filled,))
		cl.enqueue_migrate_mem_objects(self.dev2_queue, (self.dev2_proxy,), flags=cl.mem_migration_flags.CONTENT_UNDEFINED, wait_for=(dev1_filled,))
		return (dev1_filled, dev2_filled)

class CopyTo(Verifyable):
	def __init__(self, byte_size, ctx, queue1, queue2):
		self.byte_size = byte_size
		self.elem_size = byte_size_2_float_size(byte_size)
		self.dev1_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev2_buf = cl.Buffer(ctx, 0, byte_size)
		self.dev1_queue = queue1
		self.dev2_queue = queue2

	def __call__(self, waitEvents = None):
		sentinel = self.elem_size / 2
		sentinel_bytes = float_size_2_byte_size(sentinel)

		dev1_copy = cl.enqueue_copy(self.dev1_queue, self.dev2_buf, self.dev1_buf, dest_offset=0, src_offset=0, byte_count=sentinel_bytes, wait_for=waitEvents)
		dev2_copy = cl.enqueue_copy(self.dev2_queue, self.dev1_buf, self.dev2_buf, dest_offset=sentinel_bytes, src_offset=sentinel_bytes, byte_count=sentinel_bytes, wait_for=waitEvents)

		return (dev1_copy, dev2_copy)

def benchmark(implementation):

	events = None
	start = time.time()
	for i in xrange(args.num_transfers):
		events = implementation(events)
	cl.wait_for_events(events)
	end = time.time()

	avg_host = (end - start) / args.num_transfers
	bw_host = implementation.byte_size / avg_host / 1e9
	print '{0:>20}   {1:>20.0f}   {2:>20.2f}'.format(implementation.byte_size, avg_host * 1e6, bw_host)

def check(implementation):
	num_elems = implementation.elem_size
	sentinel = num_elems / 2
	for i in xrange(args.num_transfers):
		dev1_reference = np.empty(num_elems, dtype=np.float32)
		dev1_reference[:] = np.random.rand(num_elems)
		dev2_reference = np.empty(num_elems, dtype=np.float32)
		dev2_reference[:] = np.random.rand(num_elems)
		implementation.set_src(dev1_reference, dev2_reference)
		events = implementation(None)
		cl.wait_for_events(events)
		results = implementation.get_contents()
		for i_dev in xrange(len(results)):
			result = results[i_dev]
			if (result[:sentinel] != dev1_reference[:sentinel]).any():
				print 'First half of device {0} does not match source values!'.format(i_dev)
				return
			if (result[sentinel:] != dev2_reference[sentinel:]).any():
				print 'Second half of device {0} does not match source values!'.format(i_dev)
				return
	print 'No errors found.'
	return

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Benchmark PCI bandwidth')
	parser.add_argument('src', type=int, help='The device from which to send the data')
	parser.add_argument('dest', type=int, help='The device to which to send the data')
	parser.add_argument('-p', '--platform', type=int, metavar='I', help='The platform to use for the measurement (requires device to be specified)')
	parser.add_argument('-s', '--size', type=int, metavar='BYTE', help='Memory size in byte', default=4*1024*1024)
	parser.add_argument('-t', '--type', metavar='TYPE', help='The type of memory/transfer to used', default='plain')
	parser.add_argument('-n', '--num-transfers', type=int, help='How many transfers to perform', default=1000)
	parser.add_argument('-c', '--check', action='store_true', default=False, help='Instead of benchmarking check whether destination buffer contains correct data.')
	parser.add_argument('--sweep', action='store_true', default=False, help='Sweep the range of potential memory sizes')

	args = parser.parse_args()

	(ctx, queue1, queue2) = init_cl(args.platform, args.src, args.dest)

	def make_implementation(size):
		if args.type == 'plain':
			return PlainMemory(size, ctx, queue1, queue2)
		elif args.type == 'plain_blocking':
			return PlainMemoryBlocking(size, ctx, queue1, queue2)
		elif args.type == 'pinned':
			return Pinned(size, ctx, queue1, queue2)
		elif args.type == 'pinned_blocking':
			return PinnedBlocking(size, ctx, queue1, queue2)
		elif args.type == 'pinned_multihost':
			return PinnedMultiHostBuf(size, ctx, queue1, queue2)
		elif args.type == 'pio':
			return PIO(size, ctx, queue1, queue2)
		elif args.type == 'pio_threaded':
			return PIO_threaded(size, ctx, queue1, queue2)
		elif args.type == 'readMapped':
			return ReadMapped(size, ctx, queue1, queue2)
		elif args.type == 'writeMapped':
			return WriteMapped(size, ctx, queue1, queue2)
		elif args.type == 'copyFrom':
			return CopyFrom(size, ctx, queue1, queue2)
		elif args.type == 'copyFrom_save':
			return CopyFrom_save(size, ctx, queue1, queue2)
		elif args.type == 'copyFrom_proxied':
			return CopyFrom_proxied(size, ctx, queue1, queue2)
		elif args.type == 'copyFrom_proxied12':
			return CopyFrom_proxied12(size, ctx, queue1, queue2)
		elif args.type == 'copyTo':
			return CopyTo(size, ctx, queue1, queue2)
		else:
			print 'The memory/transfer type "{0}" does not exist'.format(args.type)
			sys.exit(1)

	if args.check:
		check(implementation, args.num_transfers)
	else:
		print '{0:>20}   {1:>20}   {2:>20}'.format('Buffer Size / B', 'Transfer Time / mus', 'Bandwidth / GB/s')
		if args.sweep:
			from sweep import sweep
			sweep(make_implementation, benchmark)
		else:
			implementation = make_implementation(args.size)
			benchmark(implementation)
